package com.drug.controller;

import com.drug.entity.*;
import com.drug.service.DrugService;
import com.drug.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Field;
import java.util.*;

@RestController
public class DrugController {

    @Autowired
    private DrugService drugService;
    @Autowired
    private UserService userService;
    //请求热门推荐
    @GetMapping("/queryHotList")
    public List<Drug> queryHotList(){
        List<Drug> drugList = drugService.queryHotList();
        return drugList;
    }

    //请求猜你喜欢
    @PostMapping("/queryLikeList")
    public List<Drug> queryLikeList(@RequestBody User user) throws IllegalAccessException {
        TrackLog userTrack = userService.queryUserTrack(user.getOpenId());
        //推荐药材数
        Integer max = 2;
        Random rand =new Random();
        List<Drug> likeList;
        List<Integer> likeIdList = new ArrayList<>();
        //用户行为map（标签:标签权重）
        HashMap<String,Integer> map = new HashMap<>();
        //标签所含药材ID集map（标签:药材ID集）
        HashMap<String,LinkedList<Integer>> map2 = new HashMap<>();
        //总权重
        int sum = 0;
        //遍历 加工 用户行为 里的标签
        Field[] fields = userTrack.getClass().getDeclaredFields();
        for(int i=0; i<fields.length; i++){
            Field f = fields[i];
            f.setAccessible(true);
            //取出 当前标签 下 的 所有药材ID
            LinkedList<Integer> temp = drugService.queryDrugByLabelName("label_"+f.getName().replace("label",""));
            if (!temp.isEmpty()){
                //      标签         标签对应的权重
                map.put(f.getName(), (Integer) f.get(userTrack));
                //      标签         拥有该标签的药材ID集
                map2.put(f.getName(),temp);
                //        标签权重      +      初始权重
                sum+=(Integer) f.get(userTrack)+1;
            }
        }

        for (int i=0;i<max;i++){
            //根据随机数确定标签 [0,1)
            double ran = rand.nextDouble();
            double div = 0;

            for(Map.Entry<String, Integer> entry : map.entrySet()){
                String mapKey = entry.getKey();
                Integer mapValue = entry.getValue();
                //通过累加 权重的概率 判断随机出的标签
                div += (mapValue+1.0) / sum;
                //确定随机到的标签ID
                if (ran <= div){
                    //取出标签对应的药材ID集
                    LinkedList<Integer> tempList = map2.get(mapKey);
                    //随机出药材ID Integer [0~size)
                    Integer Index = rand.nextInt(tempList.size());
                    Integer drugId = tempList.get(Index);
                    likeIdList.add(drugId);
                    System.out.println("标签："+mapKey+" 药材ID："+drugId);
                    //在map2的药材ID集中删除该药材
                    for (Iterator<Map.Entry<String, LinkedList<Integer>>> iter = map2.entrySet().iterator();iter.hasNext();) {
                        Map.Entry<String, LinkedList<Integer>> it = iter.next();
                        String key = it.getKey();
                        LinkedList<Integer> itList = it.getValue();
                        itList.remove(drugId);
                        //如果该标签已空，在map、map2删除该标签，sum减去该标签权重
                        if (itList.isEmpty()){
                            sum = sum-1-map.get(key);
                            map.remove(key);
                            iter.remove();
                        }
                    }
                    break;
                }
            }
        }

        likeList = drugService.queryDrugByDrugIdList(likeIdList);
        return likeList;
    }

    //提交用户行为
    @PostMapping("/subInfo")
    public void subInfo(@RequestBody Map<String,Object> map) throws IllegalAccessException {
        String openId = (String) map.get("openId");
        Integer drugId = (Integer) map.get("drugId");

        //更新药材热度：药材ID，增加热度值
        drugService.addDrugPop(drugId,1);

        //更新用户偏好矩阵
        TrackLog trackLog = drugService.queryDrugLabelById(drugId);
        TrackLog userTrack = userService.queryUserTrack(openId);

        Field[] fields = userTrack.getClass().getDeclaredFields();
        HashMap<String,Integer> map2 = new HashMap<>();
        for(int i=0; i<fields.length; i++) {
            Field f = fields[i];
            f.setAccessible(true);
            map2.put(f.getName(), (Integer) f.get(userTrack));
        }

        for(int i=0; i<fields.length; i++) {
            Field f = fields[i];
            f.setAccessible(true);
            if (f.get(trackLog).equals(1)){
                Integer value = map2.get(f.getName())+1;
                userService.addUserTrack(openId,"label_"+f.getName().replace("label",""),value);
            }
        }

    }

    //请求题list
    @GetMapping("/getTestList")
    public List<Test> getTestList() throws IllegalAccessException {
        //题数
        Integer testNumber = 5;
        //选项数
        Integer answerNumber = 4;

        Random rand =new Random();

        List<Test> testList = new ArrayList<>();

        List<Drug> drugs = drugService.queryDrugAllList();
        List<Label> labelList = drugService.queryLabelAll();

        //构造一道题
        for(int i=0;i<testNumber;i++){
            Test t = new Test();
            List<Answer> answers = new ArrayList<>();

            //随机出题药材 [0~size)
            Drug drug = drugs.get(rand.nextInt(drugs.size()));
            //随机出正确答案value
            Integer rightkey = rand.nextInt(answerNumber);
            //题面、正确答案value
            t.setTitle("请根据提示选择跟药材相匹配的答案\n药材名称："+drug.getDrugName()+"\n功效：\n "+drug.getDrugEffect());
            t.setRightKey(rightkey);

            //筛出正确集与错误集
            TrackLog labels = drugService.queryDrugLabelById(drug.getDrugId());

            List<Integer> rList = new ArrayList<>();
            List<Integer> eList = new ArrayList<>();
            Map<Integer,Label> mapLabel = new HashMap<>();

            for (Label label:labelList) {
                mapLabel.put(label.getLabelId(),label);
                eList.add(label.getLabelId());
            }

            Field[] fields = labels.getClass().getDeclaredFields();
            for(int j=0; j<fields.length; j++){
                Field f = fields[j];
                f.setAccessible(true);
                if ((Integer)f.get(labels)==1){
                    Integer temp = Integer.valueOf(f.getName().replace("label",""));
                    rList.add(temp);
                    eList.remove(temp);
                }
            }
            //构建N个选项
            for(int j=0;j<answerNumber;j++){
                String ans;
                Integer rLabelId;
                Label label;
                if (j==rightkey){
                    rLabelId = rList.get(rand.nextInt(rList.size()));
                    label = mapLabel.get(rLabelId);
                    rList.remove(rLabelId);
                }else{
                    rLabelId = eList.get(rand.nextInt(eList.size()));
                    label = mapLabel.get(rLabelId);
                    eList.remove(rLabelId);
                }
                ans=label.getLabelType()+"："+label.getLabelName();
                answers.add(new Answer(j,ans));
            }
            t.setAnswers(answers);
            testList.add(t);
        }

        return testList;
    }

    //新增药材
    @PostMapping("/addAllDrugList")
    public void addAllDrugList(@RequestBody Drug drug) throws Exception {
        System.out.println("新增药材");
        List<Label> allLabel = drugService.queryLabelAll();
        TrackLog drugLabel = new TrackLog();
        //提取标签
        for (Label label:allLabel) {
            //根据标签来源取出药材属性
            Field field = drug.getClass().getDeclaredField(label.getLabelFrom());
            field.setAccessible(true);
            String drugSX = (String) field.get(drug);
            //是否存在该标签
            if (drugSX.indexOf(label.getLabelName())!=-1){
                Field f = drugLabel.getClass().getDeclaredField("label"+label.getLabelId());
                f.setAccessible(true);
                f.set(drugLabel,1);
            }else{
                Field f = drugLabel.getClass().getDeclaredField("label"+label.getLabelId());
                f.setAccessible(true);
                f.set(drugLabel,0);
            }
        }
        drugService.insertDrug(drug);
        drugService.insertDrugLabel(drugLabel);
    }

    @GetMapping("/queryDrugInitialsList")
    public List<Drug> queryDrugInitialsList(@RequestParam(value="drugInitials")  String drugInitials){
        List<Drug> drugList = drugService.queryDrugInitialsList(drugInitials);
        return drugList;
    }

    @GetMapping("/vagueSearchDrugList")
    public List<Drug> vagueSearchDrugList(@RequestParam(value="vague")  String vague){
        List<Drug> drugList = drugService.vagueSearchDrugList(vague);
        return drugList;
    }

    @GetMapping("/deleteDrug")
    public void deleteDrug(@RequestParam(value = "drugId") Integer drugId){
        drugService.deleteDrug(drugId);
    }

    @GetMapping("/queryDrugById")
    public List<Drug> queryDrugById(@RequestParam(value = "drugId") Integer drugId){
        return drugService.queryDrugById(drugId);
    }

    @GetMapping("/queryDrugAllList")
    public List<Drug> queryDrugAllList(){
        List<Drug> drugList = drugService.queryDrugAllList();
        return drugList;
    }

}
