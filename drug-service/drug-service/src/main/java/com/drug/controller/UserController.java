package com.drug.controller;

import com.drug.entity.User;
import com.drug.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/insertUserList")
    public void insertUserList(@RequestBody User user){
        userService.insertUserList(user);
    }

    @GetMapping("/queryUserList")
    public List<User> queryUserList(@RequestParam(value = "openId") String openId){
        return userService.queryUserList(openId);
    }

    @GetMapping("/deleteUser")
    public void deleteUser(@RequestParam(value = "openId") String openId){
        userService.deleteUser(openId);
    }
    //查询排行榜
    @GetMapping("/queryRankList")
    public List<User> queryRankList(){
        Integer max = 10;
        return userService.queryRankList(max);
    }
    //提交答题结果
    @PostMapping("/subTestRes")
    public void subInfo(@RequestBody Map<String,Object> map){
        String openId = (String) map.get("openId");
        Integer rightNumber = (Integer) map.get("rightNumber");
        userService.addPoints(openId,rightNumber);
    }
}
