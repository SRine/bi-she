package com.drug.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class Label {
    private Integer labelId;
    private String labelName;
    private String labelType;
    private String labelFrom;

    public Integer getLabelId() {
        return labelId;
    }

    public String getLabelName() {
        return labelName;
    }

    public String getLabelType() {
        return labelType;
    }

    public String getLabelFrom() {
        return labelFrom;
    }
}
