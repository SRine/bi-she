package com.drug.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Answer {
    private Integer value;

    private String text;

    public Answer(Integer value, String text) {
        this.value = value;
        this.text = text;
    }
}
