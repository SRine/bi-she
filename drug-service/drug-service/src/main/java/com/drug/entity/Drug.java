package com.drug.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Drug {

    //  主键id
    private Integer drugId;
    //  按首字母查询
    private String drugInitials;
    //  学名
    private String drugName;
    //  别名
    private String drugAlias;
    //  来源
    private String drugSource;
    //  性味
    private String drugProperties;
    //  归经
    private String drugMeridians;
    //  功效
    private String drugEffect;
    //  用法
    private String drugUse;
    //  禁忌
    private String drugTaboo;
    //  外形特点
    private String drugContour;
    //  炮制方式
    private String drugPrepare;
    //  相克药物
    private String drugContrary;
    //  生长周期
    private String drugCycle;
    //  生长环境
    private String drugEnvironment;
    //  毒性强弱
    private String drugIntensity;
    //  毒发时间
    private String drugTime;
    //  中毒途径
    private String drugChannel;
    //  解毒方式
    private String drugMode;
    //  摘录
    private String drugExtract;
    //  图片路径
    private String imgUrl;
    //  毒性强弱
    private String toxicIntensity;
    //  毒发时间
    private String toxicTime;
    //  中毒途径
    private String toxicChannel;
    //  解毒方式
    private String toxicMode;

    public Integer getDrugId() {
        return drugId;
    }

    public String getDrugInitials() {
        return drugInitials;
    }

    public String getDrugName() {
        return drugName;
    }

    public String getDrugAlias() {
        return drugAlias;
    }

    public String getDrugSource() {
        return drugSource;
    }

    public String getDrugProperties() {
        return drugProperties;
    }

    public String getDrugMeridians() {
        return drugMeridians;
    }

    public String getDrugEffect() {
        return drugEffect;
    }

    public String getDrugUse() {
        return drugUse;
    }

    public String getDrugTaboo() {
        return drugTaboo;
    }

    public String getDrugContour() {
        return drugContour;
    }

    public String getDrugPrepare() {
        return drugPrepare;
    }

    public String getDrugContrary() {
        return drugContrary;
    }

    public String getDrugCycle() {
        return drugCycle;
    }

    public String getDrugEnvironment() {
        return drugEnvironment;
    }

    public String getDrugIntensity() {
        return drugIntensity;
    }

    public String getDrugTime() {
        return drugTime;
    }

    public String getDrugChannel() {
        return drugChannel;
    }

    public String getDrugMode() {
        return drugMode;
    }

    public String getDrugExtract() {
        return drugExtract;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getToxicIntensity() {
        return toxicIntensity;
    }

    public String getToxicTime() {
        return toxicTime;
    }

    public String getToxicChannel() {
        return toxicChannel;
    }

    public String getToxicMode() {
        return toxicMode;
    }

    public Integer getDrugPopularity() {
        return drugPopularity;
    }

    //  热度
    private Integer drugPopularity;
}
