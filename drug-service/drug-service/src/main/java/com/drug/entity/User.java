package com.drug.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {

    //    服务商唯一用户标识
    private String openId;
    //    用户昵称
    private String nickName;
    //    用户头像
    private String avatarUrl;

    //    用户性别
    private Integer gender;
    //    所在国家
    private String country;
    //    所在省份
    private String province;
    //    所在城市
    private String city;
    //    用户轨迹
    private TrackLog userTrack;
    //    总积分
    private Integer totalPoints;
    //    剩余积分
    private Integer remainPoints;


    public Integer getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(Integer totalPoints) {
        this.totalPoints = totalPoints;
    }

    public Integer getRemainPoints() {
        return remainPoints;
    }

    public void setRemainPoints(Integer remainPoints) {
        this.remainPoints = remainPoints;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public TrackLog getUserTrack() {
        return userTrack;
    }

    public void setUserTrack(TrackLog userTrack) {
        this.userTrack = userTrack;
    }
}
