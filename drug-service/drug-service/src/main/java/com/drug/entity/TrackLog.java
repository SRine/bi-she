package com.drug.entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TrackLog {

    private Integer label1;

    private Integer label2;

    private Integer label3;

    private Integer label4;

    private Integer label5;

    private Integer label6;

    private Integer label7;

    private Integer label8;

    private Integer label9;

    private Integer label10;

    private Integer label11;

    public Integer getLabel1() {
        return label1;
    }

    public void setLabel1(Integer label1) {
        this.label1 = label1;
    }

    public Integer getLabel2() {
        return label2;
    }

    public void setLabel2(Integer label2) {
        this.label2 = label2;
    }

    public Integer getLabel3() {
        return label3;
    }

    public void setLabel3(Integer label3) {
        this.label3 = label3;
    }

    public Integer getLabel4() {
        return label4;
    }

    public void setLabel4(Integer label4) {
        this.label4 = label4;
    }

    public Integer getLabel5() {
        return label5;
    }

    public void setLabel5(Integer label5) {
        this.label5 = label5;
    }

    public Integer getLabel6() {
        return label6;
    }

    public void setLabel6(Integer label6) {
        this.label6 = label6;
    }

    public Integer getLabel7() {
        return label7;
    }

    public void setLabel7(Integer label7) {
        this.label7 = label7;
    }

    public Integer getLabel8() {
        return label8;
    }

    public void setLabel8(Integer label8) {
        this.label8 = label8;
    }

    public Integer getLabel9() {
        return label9;
    }

    public void setLabel9(Integer label9) {
        this.label9 = label9;
    }

    public Integer getLabel10() {
        return label10;
    }

    public void setLabel10(Integer label10) {
        this.label10 = label10;
    }

    public Integer getLabel11() {
        return label11;
    }

    public void setLabel11(Integer label11) {
        this.label11 = label11;
    }
}
