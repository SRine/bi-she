package com.drug.mapper;

import com.drug.entity.TrackLog;
import com.drug.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface UserMapper {

    List<User> queryUserList(String openId);

    List<User> queryRankList(Integer max);

    TrackLog queryUserTrack(String openId);

    void insertUserList(User user);

    void deleteUser(String openId);

    void addUserTrack(@Param("openId")String openId, @Param("labelName") String labelName, @Param("value")Integer value);

    void addPoints(@Param("openId")String openId,@Param("value")Integer value);
}
