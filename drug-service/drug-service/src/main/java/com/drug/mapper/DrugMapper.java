package com.drug.mapper;

import com.drug.entity.Drug;
import com.drug.entity.Label;
import com.drug.entity.TrackLog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.LinkedList;
import java.util.List;

@Mapper
@Repository
public interface DrugMapper {

    List<Drug> queryHotList();

    List<Drug> queryDrugInitialsList(String drugInitials);

    List<Drug> vagueSearchDrugList(String vague);

    void deleteDrug(Integer drugId);

    List<Drug> queryDrugAllList();

    List<Drug> queryDrugByDrugIdList(List<Integer> drugIdList);

    LinkedList<Integer> queryDrugByLabelName(String labelName);

    List<Drug> queryDrugById(Integer drugId);

    void addDrugPop(@Param("drugId") Integer drugId, @Param("value") Integer value);

    TrackLog queryDrugLabelById(Integer drugId);

    List<Label> queryLabelAll();

    void insertDrug(Drug drug);
    void insertDrugLabel(TrackLog drugLabel);
}
