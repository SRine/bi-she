package com.drug.service;

import com.drug.entity.TrackLog;
import com.drug.entity.User;
import com.drug.mapper.UserMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Component
public class UserService {
    @Autowired
    private UserMapper userMapper;

    public List<User> queryUserList(String openId){
        return userMapper.queryUserList(openId);
    }

    public List<User> queryRankList(Integer max){ return userMapper.queryRankList(max);}

    public TrackLog queryUserTrack(String openId){
        return userMapper.queryUserTrack(openId);
    }

    public void insertUserList(User user){
        userMapper.insertUserList(user);
    }

    public void deleteUser(String openId){ userMapper.deleteUser(openId); }

    public void addUserTrack(String openId,String labelName,Integer value){ userMapper.addUserTrack(openId,labelName,value);}

    public void addPoints(@Param("openId")String openId, @Param("value")Integer value){ userMapper.addPoints(openId,value);}
}
