package com.drug.service;

import com.drug.entity.Drug;
import com.drug.entity.Label;
import com.drug.entity.TrackLog;
import com.drug.mapper.DrugMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@Component
public class DrugService {

    @Autowired
    private DrugMapper drugMapper;

    public List<Drug> queryHotList(){return drugMapper.queryHotList(); }

    public LinkedList<Integer> queryDrugByLabelName(String labelName){ return drugMapper.queryDrugByLabelName(labelName); }

    public List<Drug> queryDrugInitialsList(String drugInitials) { return drugMapper.queryDrugInitialsList(drugInitials); }

    public List<Drug> queryDrugByDrugIdList(List<Integer> drugIdList){return  drugMapper.queryDrugByDrugIdList(drugIdList);}

    public List<Drug> vagueSearchDrugList(String vague) {
        return drugMapper.vagueSearchDrugList(vague);
    }

    public void deleteDrug(Integer drugId){
        drugMapper.deleteDrug(drugId);
    }

    public List<Drug> queryDrugAllList(){
        return drugMapper.queryDrugAllList();
    }

    public List<Drug> queryDrugById(Integer drugId) {
        return drugMapper.queryDrugById(drugId);
    }

    public void addDrugPop( Integer drugId,Integer value){ drugMapper.addDrugPop(drugId,value); }

    public TrackLog queryDrugLabelById(Integer drugId){ return drugMapper.queryDrugLabelById(drugId); }
    public List<Label> queryLabelAll(){ return  drugMapper.queryLabelAll();}

    public void insertDrug(Drug drug){drugMapper.insertDrug(drug);}
    public void insertDrugLabel(TrackLog drugLabel){drugMapper.insertDrugLabel(drugLabel);}
}
