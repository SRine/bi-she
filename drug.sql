/*
Navicat MySQL Data Transfer

Source Server         : his
Source Server Version : 80025
Source Host           : localhost:3306
Source Database       : drug

Target Server Type    : MYSQL
Target Server Version : 80025
File Encoding         : 65001

Date: 2022-05-09 20:05:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `alldrug`
-- ----------------------------
DROP TABLE IF EXISTS `alldrug`;
CREATE TABLE `alldrug` (
  `drug_id` int NOT NULL AUTO_INCREMENT,
  `drug_initials` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `drug_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `drug_alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `drug_source` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `drug_properties` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `drug_meridians` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `drug_effect` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `drug_use` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `drug_taboo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `drug_contour` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `drug_prepare` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `drug_contrary` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `drug_cycle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `drug_environment` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `toxic_intensity` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `toxic_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `toxic_channel` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `toxic_mode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `drug_extract` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `img_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `drug_popularity` int DEFAULT '0',
  PRIMARY KEY (`drug_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of alldrug
-- ----------------------------
INSERT INTO `alldrug` VALUES ('1', 'F', '茯苓', '茯菟（《本经》），茯灵（《史记》），茯蕶（《广雅》），伏苓、伏菟（《唐本草》），松腴（《记事珠》），绛晨伏胎（《酉阳杂俎》），云苓（《滇海虞衡志》），茯兔（《纲目》），松薯、松木薯，松苓（《广西中药志》）。', '为多孔菌科植物茯苓的干燥菌核。', '甘、淡，平。', null, '利水渗湿，健脾，安神。水肿尿少，痰饮眩悸，脾虚食少，便溏泄泻，心神不安，惊悸失眠。现有用于子宫肌瘤的治疗(桂枝茯苓胶囊)。 ', '10-15g。用于安神可以与朱砂拌用，处方写朱茯苓或朱衣茯苓。    ', null, '菌核球形、卵形、椭圆形至不规则形，长10-30cm或者更长，重量也不等，一般重500-5000g。外面吸厚而多皱褶的皮壳，深褐色，新鲜时软干 后变硬；内部白色或淡粉红色，粉粒状。子实体生于菌核表面，全平伏，厚3-8cm，白色，肉质，老后或干后变为浅褐色。菌管密长2-3mm管壁薄，管口圆形、多角形或不规则形，径0.5-1.5cm，口缘裂为齿状。孢子长方形至近圆柱形，平滑，有一歪尖，大小（7.5-9)μm×（3-3.5）μm。', null, null, '野生茯苓一般在7月至次年3月间到马尾松林中采取。', '寄生于松科植物赤松或马尾松等树根上，深入地下20～30厘米。分布河北、河南、山东、安徽、浙江、福建、广东、广西、湖南、湖北、四川、贵州、云南、山西等地。\r\n\r\n主产安徽、湖北、河南、云南。此外贵州、四川、广西、福建、湖南、浙江、河北等地亦产。以云南所产品质较佳，安徽、湖北产量较大。', null, null, null, null, '《*辞典》', '../../static/hot/茯苓.jpg', '32');
INSERT INTO `alldrug` VALUES ('2', 'Y', '银杏', '白果、公孙树、鸭脚子、鸭掌树', '五加科植物人参Panax ginseng C． A． Mey．的干燥根。栽培者为园参，野生者为山参。多于秋季采挖，洗净；园参经晒干或烘干，称生晒参；山参经晒干，称生晒山参；经水烫，浸糖后干燥，称白糖参；蒸熟后晒干或烘干，称红参。', '平、甘、苦。', '入肺、肾经。', '熟食温肺益气，定喘嗽，缩小便，止白浊；生食降痰消毒、杀虫。促进血液循环，保护细胞免受伤害，并将自由基清除出体外。', '', null, '乔木，高达40米，胸径可达4米；幼树树皮浅纵裂，大树之皮呈灰褐色，深纵裂，粗糙；幼年及壮年树冠圆锥形，老则广卵形。叶扇形，有长柄，淡绿色，无毛，有多数叉状并列细脉，顶端宽5-8厘米，在短枝上常具波状缺刻，在长枝上常2裂，基部宽楔形。球花雌雄异株，单性，生于短枝顶端的鳞片状叶的腋内，呈簇生状；雄球花葇荑花序状，下垂。种子具长梗，下垂，常为椭圆形、长倒卵形、卵圆形或近圆球形状。', null, null, null, '仅浙江天目山有野生状态的树木，生于海拔500-1000米、酸性（pH值5-5.5）黄壤、排水良好地带的天然林中，常与柳杉、榧树、蓝果树等针阔叶树种混生，生长旺盛。朝鲜、日本及欧美各国庭园均有栽培。', null, null, null, null, null, '../../static/hot/银杏.jpg', '16');
INSERT INTO `alldrug` VALUES ('3', 'L', '连翘', '黄花杆，黄寿丹', null, '苦，凉。', '入心、肝、胆经。', '清热，解毒，散结，消肿。治温热，丹毒，斑疹，痈疡肿毒，瘰疬，小便淋闭。', '内服：煎汤，3-5钱；或入丸，散。外用：煎水洗。', '脾胃虚弱，气虚发热，痈疽已溃、脓稀色淡者忌服。', '连翘早春先叶开花，花开香气淡艳，满枝金黄，艳丽可爱，是早春优良观花灌木，株高可达3米，枝干丛生，小枝黄色，拱形下垂，中空。叶对生，单叶或三小叶，卵形或卵状椭圆形，缘具齿。花冠黄色，1-3朵生于叶腋；果卵球形、卵状椭圆形或长椭圆形，先端喙状渐尖，表面疏生皮孔；果梗长0.7-1.5厘米。', null, null, '花期3-4月，果期7-9月。', '生长于山坡灌丛、林下或草丛中，或山谷、山沟疏林中，海拔250-2200米。果实可以入药。产于中国河北、山西、陕西、山东、安徽西部、河南、湖北、四川。', null, null, null, null, null, '../../static/hot/连翘.jpg', '13');
INSERT INTO `alldrug` VALUES ('4', 'S', '石斛', '仙斛兰韵、不死草、还魂草、紫萦仙株、吊兰、林兰、禁生等', null, '甘，微寒。', '归胃、肾经。', '益胃生津，滋阴清热。用于阴伤津亏，口干烦渴，食少干呕，病后虚热，目暗不明。   ', '6～12g，鲜品15～30g。入复方宜先煎，单用可久煎。', null, '茎直立，肉质状肥厚，稍扁的圆柱形，长10~60厘米，粗达1.3厘米。药用植物，性味甘淡微咸，寒，归胃、肾，肺经。益胃生津，滋阴清热 [1]  。用于阴伤津亏，口干烦渴，食少干呕，病后虚热，目暗不明。石斛花姿优雅，玲珑可爱，花色鲜艳，气味芳香，被喻为“四大观赏洋花”之一', null, null, null, '石斛喜在温暖、潮湿、半阴半阳的环境中生长，以年降雨量1000毫米以上、空气湿度大于80%、 1月平均气温高于8℃的亚热带深山老林中生长为佳，对土肥要求不甚严格，野生多在疏松且厚的树皮或树干上生长，有的也生长于石缝中', null, null, null, null, null, '../../static/hot/石斛.jpg', '33');

-- ----------------------------
-- Table structure for `alllabel`
-- ----------------------------
DROP TABLE IF EXISTS `alllabel`;
CREATE TABLE `alllabel` (
  `label_id` int NOT NULL AUTO_INCREMENT,
  `label_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `label_type` varchar(255) DEFAULT NULL,
  `label_from` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`label_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of alllabel
-- ----------------------------
INSERT INTO `alllabel` VALUES ('1', '辛', '味', 'drugProperties');
INSERT INTO `alllabel` VALUES ('2', '甘', '味', 'drugProperties');
INSERT INTO `alllabel` VALUES ('3', '酸', '味', 'drugProperties');
INSERT INTO `alllabel` VALUES ('4', '苦', '味', 'drugProperties');
INSERT INTO `alllabel` VALUES ('5', '咸', '味', 'drugProperties');
INSERT INTO `alllabel` VALUES ('6', '淡', '味', 'drugProperties');
INSERT INTO `alllabel` VALUES ('7', '寒', '性', 'drugProperties');
INSERT INTO `alllabel` VALUES ('8', '热', '性', 'drugProperties');
INSERT INTO `alllabel` VALUES ('9', '温', '性', 'drugProperties');
INSERT INTO `alllabel` VALUES ('10', '凉', '性', 'drugProperties');
INSERT INTO `alllabel` VALUES ('11', '平', '性', 'drugProperties');

-- ----------------------------
-- Table structure for `drug_label`
-- ----------------------------
DROP TABLE IF EXISTS `drug_label`;
CREATE TABLE `drug_label` (
  `drug_id` int NOT NULL AUTO_INCREMENT,
  `label_1` int DEFAULT '0',
  `label_2` int DEFAULT '0',
  `label_3` int DEFAULT '0',
  `label_4` int DEFAULT '0',
  `label_5` int DEFAULT '0',
  `label_6` int DEFAULT '0',
  `label_7` int DEFAULT '0',
  `label_8` int DEFAULT '0',
  `label_9` int DEFAULT '0',
  `label_10` int DEFAULT '0',
  `label_11` int DEFAULT '0',
  PRIMARY KEY (`drug_id`),
  CONSTRAINT `drug_id` FOREIGN KEY (`drug_id`) REFERENCES `alldrug` (`drug_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of drug_label
-- ----------------------------
INSERT INTO `drug_label` VALUES ('1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '1');
INSERT INTO `drug_label` VALUES ('2', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '1');
INSERT INTO `drug_label` VALUES ('3', '0', '0', '0', '1', '0', '0', '0', '0', '0', '1', '0');
INSERT INTO `drug_label` VALUES ('4', '0', '1', '0', '0', '0', '0', '1', '0', '0', '0', '0');

-- ----------------------------
-- Table structure for `track_log`
-- ----------------------------
DROP TABLE IF EXISTS `track_log`;
CREATE TABLE `track_log` (
  `open_id` varchar(255) NOT NULL,
  `label_1` int DEFAULT '0',
  `label_2` int DEFAULT '0',
  `label_3` int DEFAULT '0',
  `label_4` int DEFAULT '0',
  `label_5` int DEFAULT '0',
  `label_6` int DEFAULT '0',
  `label_7` int DEFAULT '0',
  `label_8` int DEFAULT '0',
  `label_9` int DEFAULT '0',
  `label_10` int DEFAULT '0',
  `label_11` int DEFAULT '0',
  PRIMARY KEY (`open_id`),
  CONSTRAINT `tracking_user` FOREIGN KEY (`open_id`) REFERENCES `userinfo` (`open_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of track_log
-- ----------------------------
INSERT INTO `track_log` VALUES ('oc9j16b4aCMSm0WiCDeaCmdolo20', '0', '3', '0', '1', '0', '0', '2', '0', '0', '0', '1');
INSERT INTO `track_log` VALUES ('oc9j16QEl91Rjo6HB0T7aUcxRHCI', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `track_log` VALUES ('og2074xJIlPRRoCaKwtPi31G430A', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

-- ----------------------------
-- Table structure for `userinfo`
-- ----------------------------
DROP TABLE IF EXISTS `userinfo`;
CREATE TABLE `userinfo` (
  `open_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nick_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `avatar_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `gender` int DEFAULT NULL,
  `country` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `province` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `total_points` int NOT NULL DEFAULT '0',
  `remain_points` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`open_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of userinfo
-- ----------------------------
INSERT INTO `userinfo` VALUES ('oc9j16b4aCMSm0WiCDeaCmdolo20', '吴下阿蒙', 'https://thirdwx.qlogo.cn/mmopen/vi_32/YO3DdayxybhruTjUIE8Bia1TSb1t99k639fWjnJic3DHq44LaXNAzycJsRolkFacLF3ia2ayic08RCjpEKuib5GicYicQ/132', '0', '', '', '', '11', '11');
INSERT INTO `userinfo` VALUES ('oc9j16QEl91Rjo6HB0T7aUcxRHCI', 'L.Yang', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIjXR8e20iarUXCgaKyQg3uYfKoj3LQQKxnIfZj18vGoZib6bYYmc6adibkqKDWfrmcKmnmuTqicoMdtw/132', '0', '', '', '', '3', '3');
INSERT INTO `userinfo` VALUES ('og2074xJIlPRRoCaKwtPi31G430A', '(๑•ั็ω•็ั๑)', 'https://thirdwx.qlogo.cn/mmopen/vi_32/PiajxSqBRaEIbRibKjbJ1wXxDDBcyLYibrWn22usC9Yh3lryibRhaqm5uN6HicxckQE4b14ILLz91zMWVgv6dnojPdA/132', '0', '', '', '', '36', '36');
